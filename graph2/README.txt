A Pen created at CodePen.io. You can find this one at http://codepen.io/Em-Ant/pen/PZQePP.

 D3 Challenge #4 for www.freecodecamp.com

User Stories :

* I can see a Force-directed Graph that shows which campers are posting links on Camper News to which domains.
* I can see each camper's icon on their node.
* I can see the relationship between the campers and the domains they're posting.
* I can tell approximately many times campers have linked to a specific domain from it's node size.
* I can tell approximately how many times a specific camper has posted a link from their node's size.

DOUBLE CLICK ON A NODE TO OPEN THE LINK !!!!


WARNING:  freeCodeCamp Camper News is going to be closed. I saved a snapshot of one of the last datasets, in order to keep this legacy project working.
Links to discussion maybe inactive.