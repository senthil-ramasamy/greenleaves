var select = function(s) {
    return document.querySelector(s);
  },
  selectAll = function(s) {
    return document.querySelectorAll(s);
  },  
      circle_1 = select("#Circle-1");
      lines_1 = selectAll("#lines-1 line");
      circle_2 = select("#Circle-2");
      lines_2 = selectAll("#lines-2 line");

      circle_3 = select("#Circle-3");
      lines_3= selectAll("#lines-3 line");

      circle_4 = select("#Circle-4");
      lines_4= selectAll("#lines-4 line");
    line_first=select("#line-first");
    line_1=select("#line-1");
    line_2=select("#line-2");
    line_3=select("#line-3");

  
      container = select('#container');

TweenMax.set([lines_1,lines_2,lines_3,lines_4],{
  drawSVG: '40% 40%'
})
TweenMax.set([circle_1,circle_2,circle_3,circle_4],{
  drawSVG: '0%'
})
TweenMax.set([line_first,line_1,line_2,line_3],{
  drawSVG: '0%'
})

var tl = new TimelineMax({repeat:-1,repeatDelay:3})
tl.to(line_first,0.5,{
  drawSVG: 'true'
},'-=0.5')
tl.to(circle_1,0.7,{
  drawSVG: 'true'
})
tl.to(lines_1,0.3,{
      drawSVG:'40% 70%'
      })
.to(lines_1,0.5,{
  drawSVG: '100% 100%'
})
tl.to(line_1,0.5,{
  drawSVG: 'true'
},'-=0.5')
tl.to(circle_4,0.7,{
  drawSVG: 'true'
})
tl.to(lines_4,0.3,{
      drawSVG:'40% 70%'
      })
.to(lines_4,0.5,{
  drawSVG: '100% 100%'
})
tl.to(line_2,0.5,{
  drawSVG: 'true'
},'-=0.5')
tl.to(circle_2,0.7,{
  drawSVG: 'true'
})
tl.to(lines_2,0.3,{
      drawSVG:'40% 70%'
      })
.to(lines_2,0.5,{
  drawSVG: '100% 100%'
})
tl.to(line_3,0.5,{
  drawSVG: 'true'
},'-=0.5')
tl.to(circle_3,0.7,{
  drawSVG: 'true'
})
tl.to(lines_3,0.3,{
      drawSVG:'40% 70%'
      })
.to(lines_3,0.5,{
  drawSVG: '100% 100%'
})